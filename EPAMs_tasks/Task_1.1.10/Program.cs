﻿using System;

namespace Task_1._1._10 {
    internal class Program {
        public static void Main(string[] args) {
            const int min = -100;
            const int max = 100;

            var numArray = new int[100, 100];

            var randNum = new Random();
            for (var i = 0; i < numArray.GetLength(0); i++) {
                for (var j = 0; j < numArray.GetLength(1); j++) {
                    numArray[i, j] = randNum.Next(min, max);
                }
            }

            var sum = 0;
            for (var i = 0; i < numArray.GetLength(0); i++) {
                for (var j = 0; j < numArray.GetLength(1); j++) {
                    if ((i + j) % 2 == 0) {
                        sum += numArray[i, j];
                    }
                }
            }

            Console.WriteLine(sum);
        }
    }
}