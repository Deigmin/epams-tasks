﻿using System;

namespace Task_1._1._5 {
    internal class Program {
        public static void Main(string[] args) {
            var sum = 0;
            for (var i = 0; i < 1000; i++) {
                if (i % 3 == 0 || i % 5 == 0) {
                    sum += i;
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine($"Sum is {sum}");
        }
    }
}