﻿using System;

namespace Task_1._2._4 {
    internal class Program {
        public static void Main(string[] args) {
            string sentence =
                "я плохо учил русский язык. и забываю начинать предложения с заглавной? хорошо, что можно написать программу! или нет?";

            string[] words = sentence.Split(new char[] {'.', '?', '!'});
            
            foreach (string word in words) {
                Console.WriteLine(word.TrimStart()[0].ToString().ToUpper() + word.TrimStart().Substring(1));
            }
        }
    }
}