﻿using System;

namespace Task_1._1._3 {
    internal class Program {
        public static void Main(string[] args) {
            Console.WriteLine("Enter Number of rows");
            int.TryParse(Console.ReadLine(), out var size);

            if (size <= 0 || size > 1000) {
                throw new Exception("Wrong Number");
            }
            else {
                if (size % 2 == 0) {
                    size++;
                }

                for (var i = 0; i < size; i += 2) {
                    for (var j = 0; j < (size - 1) / 2 - (i / 2); j++) {
                        Console.Write(' ');
                    }

                    for (var j = 0; j < i + 1; j++) {
                        Console.Write('*');
                    }

                    Console.WriteLine();
                }
            }
        }
    }
}