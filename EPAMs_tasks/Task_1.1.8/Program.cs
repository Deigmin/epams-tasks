﻿using System;

namespace Task_1._1._8 {
    internal class Program {
        public static void Main(string[] args) {
            const int Min = -200;
            const int Max = 200;
            int[,,] array = new int[10, 10, 10];
            var randNum = new Random();

            for (var i = 0; i < array.GetLength(0); i++) {
                for (var j = 0; j < array.GetLength(1); j++) {
                    for (var k = 0; k < array.GetLength(2); k++) {
                        array[i, j, k] = randNum.Next(Min, Max);
                        if (array[i, j, k] > 0) {
                            array[i, j, k] = 0;
                        }
                    }
                }
            }

            for (int i = 0; i < array.GetLength(0); i++) {
                for (int j = 0; j < array.GetLength(1); j++) {
                    for (int k = 0; k < array.GetLength(2); k++) {
                        Console.WriteLine(array[i, j, k]);
                    }
                }
            }
        }
    }
}