﻿using System;

namespace Task_1._2._2 {
    internal class Program {
        public static void Main(string[] args) {
            const string sentence = "написать программу, которая";
            const string needle = "описание";

            string res = "";
            foreach (var c in sentence) {
                if (needle.IndexOf(c) > -1)
                    res += new string(c, 2);
                else
                    res += c;
            }

            Console.WriteLine(res);
        }
    }
}