﻿using System;
using CharsAsString;

namespace ConsoleApplication2
{
    internal class Program 
    {
        public static void Main(string[] args) 
        {
            
            char[] helloChar = { 'H', 'e', 'l', 'l', 'o' };
            char[] worldChar = { 'W', 'o', 'r', 'l', 'd' };

            CustomString firstString = new CustomString(helloChar);
            CustomString secondString = new CustomString(worldChar);
        }
    }
}