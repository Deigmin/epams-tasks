﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Task._2._1._2 {
    class Program {
        static void Main(string[] args) {
            List<Shape> figures = new List<Shape>();
            Menu(figures);
            Console.WriteLine("Пока");
        }

        private static void Menu(List<Shape> figures) {
            var сondition = true;
            while (сondition) {
                Console.WriteLine("Выберете действие:");
                Console.WriteLine("1. Добавить фигуру");
                Console.WriteLine("2. Вывести фигуры");
                Console.WriteLine("3. Очистить холст");
                Console.WriteLine("4. Выход");
                var choise = Console.ReadLine();
                switch (choise) {
                    case "1":
                        figures.Add(AddFigure());
                        break;
                    case "2":
                        foreach (var figure in figures) {
                            Console.WriteLine(figure);
                        }
                        break;
                    case "3":
                        figures.Clear();
                        Console.WriteLine("Успешно очищено");
                        break;
                    case "4":
                        сondition = false;
                        break;
                    default:
                        Console.WriteLine("Введено ошибочное число");
                        break;
                }
            }
        }

        private static Shape AddFigure() {
            Console.WriteLine("Выберите тип фигуры:");
            Console.WriteLine("1. Круг");
            Console.WriteLine("2. Кольцо");
            Console.WriteLine("3. Прямоугольник");
            Console.WriteLine("4. Квадрат");
            Console.WriteLine("5. Треугольник");

            var choise = Console.ReadLine();
            switch (choise) {
                case "1":
                    Console.WriteLine("Введите диаметр круга");
                    var diameterStr = Console.ReadLine();

                    if (int.TryParse(diameterStr, out int diameter) && diameter > 0) {
                        return new Circle(diameter);
                    }
                    else {
                        throw new Exception("Введен неправильный диаметр");
                    }
                case "2":
                    Console.WriteLine("Введите внешний диаметр кольца");
                    var extDiameterStr = Console.ReadLine();
                    
                    Console.WriteLine("Введите внутренний диаметр кольца");
                    var intDiameterStr = Console.ReadLine();

                    if (int.TryParse(extDiameterStr, out int extDiameter) && extDiameter > 0 && int.TryParse(intDiameterStr, out int intDiameter) && extDiameter > intDiameter) {
                        return new Ring(extDiameter, intDiameter);
                    }
                    else {
                        throw new Exception("Введены неправильные диаметры");
                    }
                case "3":
                    Console.WriteLine("Введите первую сторону прямоугольника");
                    var aSideStr = Console.ReadLine();
                    
                    Console.WriteLine("Введите вторую сторону прямоугольника");
                    var bSideStr = Console.ReadLine();

                    if (int.TryParse(aSideStr, out int aSide) && int.TryParse(bSideStr, out int bSide) && aSide > 0 && bSide > 0) {
                        return new Rectangle(aSide, bSide);
                    }
                    else {
                        throw new Exception("Введены неправильные стороны");
                    }
                case "4":
                    Console.WriteLine("Введите сторону квадрата");
                    var sideStr = Console.ReadLine();

                    if (int.TryParse(sideStr, out int side) && side > 0) {
                        return new Square(side);
                    }
                    else {
                        throw new Exception("Введены неправильные стороны");
                    }
                case "5":
                    Console.WriteLine("Введите сторону треугольника");
                    sideStr = Console.ReadLine();

                    if (int.TryParse(sideStr, out side) && side > 0) {
                        return new EquilateralTriangle(side);
                    }
                    else {
                        throw new Exception("Введены неправильные стороны");
                    }
                default:
                    Console.WriteLine("Введено ошибочное число");
                    break;
            }

            return null;
        }
    }


    public interface ICoords {
        int X { get; set; }
        int Y { get; set; }
    }

    public interface IProperties {
        double Perimeter();
        double Square();
    }

    public abstract class Shape : ICoords, IProperties {
        public int X { get; set; }
        public int Y { get; set; }

        public Shape() {
        }

        public abstract double Perimeter();
        public abstract double Square();
    }

    public abstract class CircleShape : Shape {
        protected CircleShape() {
        }

        public abstract override double Perimeter();
        public abstract override double Square();
    }

    public class Circle : CircleShape {
        private int _diameter;

        public Circle(int diameter) {
            _diameter = diameter;
        }

        public override double Perimeter() {
            return Math.PI * _diameter;
        }

        public override double Square() {
            return Math.PI * Math.Pow(_diameter / 2, y: 2);
        }
    }

    public class Ring : Circle {
        private int _internalDiameter;

        public Ring(int externalDiameter, int internalDiameter) : base(externalDiameter) {
            if (externalDiameter < internalDiameter) {
                throw new Exception();
            }
            else {
                _internalDiameter = internalDiameter;
            }
        }

        public override double Square() {
            return base.Square() - Math.PI * Math.Pow(_internalDiameter / 2, y: 2);
        }

        public double RingSumLength() {
            return base.Perimeter() + Math.PI * _internalDiameter;
        }
    }

    public abstract class RectangleShape : Shape {
        public int ASide { get; }
        public int BSide { get; }

        public RectangleShape(int aSide, int bSide) {
            ASide = aSide;
            BSide = bSide;
        }
    }

    public class Rectangle : RectangleShape {
        public Rectangle(int aSide, int bSide) : base(aSide, bSide) {
        }

        public override double Perimeter() {
            return (ASide + BSide) * 2;
        }

        public override double Square() {
            return ASide * BSide;
        }
    }

    public class Square : Rectangle {
        public Square(int aSide) : base(aSide, aSide) {
        }
    }

    public abstract class TriangleShape : Shape {
        public int ASide { get; }
        private int BSide { get; }
        private int CSide { get; }

        public TriangleShape(int aSide, int bSide, int cSide) {
            ASide = aSide;
            BSide = bSide;
            CSide = cSide;
        }

        public abstract override double Perimeter();

        public abstract override double Square();
    }

    public class EquilateralTriangle : TriangleShape {
        public double Height { get; }

        public EquilateralTriangle(int aSide) : base(aSide, aSide, aSide) {
            Height = Math.Sqrt(3) * ASide / 2;
        }

        public override double Perimeter() {
            return ASide * 3;
        }

        public override double Square() {
            return 1 / 2 * ASide * Height;
        }
    }
}