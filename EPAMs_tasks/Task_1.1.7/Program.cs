﻿using System;

namespace Task_1._1._7 {
    internal class Program {
        public static void Main(string[] args) {
            const int Min = 0;
            const int Max = 100;

            var numArray = new int[100];

            var randNum = new Random();
            for (var i = 0; i < numArray.Length; i++) {
                numArray[i] = randNum.Next(Min, Max);
            }

            int temp;
            for (var i = 0; i < numArray.Length - 1; i++) {
                for (var j = i + 1; j < numArray.Length; j++) {
                    if (numArray[i] > numArray[j]) {
                        temp = numArray[i];
                        numArray[i] = numArray[j];
                        numArray[j] = temp;
                    }
                }
            }

            foreach (var num in numArray) {
                Console.WriteLine(num);
            }
            Console.WriteLine($"Min number is {numArray[Min]}");
            Console.WriteLine($"Max number is {numArray[Max - 1]}");
        }
    }
}