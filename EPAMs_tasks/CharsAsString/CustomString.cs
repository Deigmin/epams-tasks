﻿using System;

namespace CharsAsString 
{
    public class CustomString
        {
            public char[] MyString { get; }

            public int Length => MyString.Length;
            
            public CustomString(char[] myString) 
            {
                this.MyString = myString;
            }
            
            //Compare current string with anoter.
            public bool Compare(CustomString comparableString) 
            {
                if (MyString.Length == comparableString.Length) 
                {
                    for (var i = 0; i < MyString.Length; i++) 
                    {
                        if (MyString[i] != comparableString.MyString[i]) 
                        {
                            return false;
                        }
                    }

                    return true;
                }
                return false;
            }

            //Concatenation two strings together
            public CustomString Concat(CustomString concatString) 
            {
                var i = 0;
                char[] concatArray = new char[MyString.Length + concatString.Length + 1];
                for (; i < MyString.Length; i++) 
                {
                    concatArray[i] = MyString[i];
                }
                
                for (int j = 0; j < concatString.Length; j++, i++) 
                {
                    concatArray[i + 1] = concatString.MyString[j];
                }
                return new CustomString(concatArray);
            }

            //Find one symbol in string
            public bool FindSymbol(char needle) 
            {
                char[] str = MyString;
                needle = Char.ToLower(needle);
                
                for (int i = 0; i < MyString.Length; i++) 
                {
                    str[i] = Char.ToLower(str[i]);
                }
                
                for (int i = 0; i < MyString.Length; i++)
                {
                    if (str[i] == needle) 
                    {
                        return true;
                    }
                }

                return false;
            }

            public static CustomString Reverse(CustomString reverseString)  
            {
                char[] temp = new char[reverseString.Length];
                for (int i = 0; i < reverseString.Length; i++)
                {
                    temp[i] = reverseString.MyString[(reverseString.Length - 1) - i];
                }
                return new CustomString(temp);
            }

            public static string ToString(CustomString toString) 
            {
                var str = "";
                for (var i = 0; i < toString.Length; i++) 
                {
                    str += toString.MyString[i];
                }
                return str;
            }
        }
}