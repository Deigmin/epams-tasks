﻿using System;

namespace Task_1._2._3 {
    internal class Program {
        public static void Main(string[] args) {
            string sentence = "Антон выпил кофе и послушал Стинга";
            sentence = sentence.Replace(",", " ").Replace(";", " ").Replace(":", " ").Trim();
            string[] words = sentence.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            int sum = 0;
            foreach (var word in words) {
                if (char.IsLower(word[0]))
                    sum += 1;
            }

            Console.WriteLine(sum);

            sentence = "Антон хорошо начал утро: послушал Стинга, выпил кофе и посмотрел Звёздные Войны";
            sentence = sentence.Replace(",", " ").Replace(";", " ").Replace(":", " ").Trim();
            words = sentence.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            sum = 0;
            foreach (var word in words) {
                if (char.IsLower(word[0]))
                    sum += 1;
            }

            Console.WriteLine(sum);
        }
    }
}