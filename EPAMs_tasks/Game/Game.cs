﻿using System;

namespace Game {
    class Game {
        static void Main(string[] args) {
            InitialGame();
            
        }

        public static void InitialGame() {
            DrawField();
            MainCharacter mainCharacter = CreateMainCharacter();
            Console.WriteLine("How many enemies do you want to create?");
            Enemy[] enemy = CreateEnemies();
            Bonus[] bonuses = CreateBonus();
            Barrier[] barrier = CreateBarrier();
        }

        private static bool DrawField() {
            return true;
        }

        private static MainCharacter CreateMainCharacter() {
            return new MainCharacter(0, 0);
        }

        private static Enemy[] CreateEnemies() {
            return new Enemy[] {new BlueEnemy(1, 4), new GrayEnemy(4, 6), new GreenEnemy(5,1), new YellowEnemy(5, 5)};
        }
        
        private static Bonus[] CreateBonus() {
            return new Bonus[] {new Cherry(5, 7, 10), new Watermelon(7, 9, 10)};
        }
        private static Barrier[] CreateBarrier() {
            return new Barrier[] {new Barrier(7, 19), };
        }
        
    }
}