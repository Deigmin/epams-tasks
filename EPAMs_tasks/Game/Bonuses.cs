﻿using System;

namespace Game {
    interface IPosition {
    int X { get; }
    int Y { get; }
    }
    public abstract class Bonus : IPosition {
        public int X { get; }
        public int Y { get; }

        public Bonus(int x, int y) {
            X = x;
            Y = y;
        }
    }

    public class Cherry : Bonus {
        public int Damage { get; }

        public Cherry(int x, int y, int damage) : base(x, y) {
            Damage = damage;
        }
    }

    public class Watermelon : Bonus {
        public int Health { get; }

        public Watermelon(int x, int y, int health) : base(x, y) {
            Health = health;
        }
    }
}