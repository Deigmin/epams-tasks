﻿namespace Game {
    public class Barrier : IPosition {
        public int X { get; }
        public int Y { get; }

        public Barrier(int x, int y) {
            X = x;
            Y = y;
        }
    }
}