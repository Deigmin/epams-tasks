﻿using System;

namespace Game {
    public interface IMovable {
        void Move(int x, int y);
    }

    public interface IMortable {
        void Death(Character character);
    }
    
    public abstract class Character : IMovable, IMortable {
        private int _x;
        private int _y;
        public int Health { get; set; } = 100;
        public int Damage { get; set; } = 1;

        public Character(int x, int y) {
            _x = x;
            _y = y;
        }

        public void Move(int x, int y) {
            _x += x;
            _y += y;
        }
        
        //idk how to realize death without transmitting character-obj, help
        public void Death(Character character) {
            character = null;
        }
    }


    public class MainCharacter : Character {
        public MainCharacter(int x, int y) : base(x, y) {
        }
    }

    public abstract class Enemy : Character {
        public Enemy(int x, int y) : base(x, y) {
        }
    }

    public abstract class ColourEnemy : Enemy {
        public string Colour { get; }

        public ColourEnemy(int x, int y, string colour) : base(x, y) {
            Colour = colour;
            Console.WriteLine($"{Colour} enemy was created");
        }
    }

    public class GreenEnemy : ColourEnemy {
        public GreenEnemy(int x, int y) : base(x, y, "Green") {
        }
    }

    public class YellowEnemy : ColourEnemy {
        public YellowEnemy(int x, int y) : base(x, y, "Yellow") {
        }
    }

    public class GrayEnemy : ColourEnemy {
        public GrayEnemy(int x, int y) : base(x, y, "Gray") {
        }
    }

    public class BlueEnemy : ColourEnemy {
        public BlueEnemy(int x, int y) : base(x, y, "Blue") {
        }
    }
}