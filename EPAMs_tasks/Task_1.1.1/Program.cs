﻿using System;

namespace Task1 {
    internal class Program {
        public static void Main(string[] args) {
            Console.WriteLine("Enter a-side of a rectangle \n");
            var sideA = Console.ReadLine();
            Console.WriteLine("Enter b-side of a rectangle");
            var sideB = Console.ReadLine();

            int.TryParse(sideA, out var aResult);
            int.TryParse(sideB, out var bResult);

            if (aResult <= 0 || bResult <= 0) {
                Console.WriteLine("Numbers must be more than zero");
            }
            else if (aResult > 0 && bResult > 0) {
                Console.WriteLine($"The Square of rectangle is {aResult * bResult}");
            }
        }
    }
}