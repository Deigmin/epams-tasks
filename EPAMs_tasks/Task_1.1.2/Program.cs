﻿using System;

namespace Task2 {
    internal class Program {
        public static void Main(string[] args) {
            Console.WriteLine("Enter number of rows");
            int.TryParse(Console.ReadLine(), out var quantity);
            if (quantity <= 0 || quantity > 1000) {
                Console.WriteLine("Wrong Number");
            }
            else {
                for (var i = 0; i < quantity; i++) {
                    for (var j = 0; j < i + 1; j++) {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}