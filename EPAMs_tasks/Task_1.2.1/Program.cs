﻿using System;

namespace Task_1._2._1 {
    internal class Program {
        public static void Main(string[] args) {
            string text =
                "Викентий хорошо отметил день рождения: покушал пиццу, посмотрел кино, пообщался со студентами в чате";

            string[] words = text.Split(new char[] {' '});
            double sum = 0;
            foreach (string word in words) {
                sum += word.Length;
            }
            //Leave as is 
            sum /= words.Length;
            Console.WriteLine(sum);
        }
    }
}