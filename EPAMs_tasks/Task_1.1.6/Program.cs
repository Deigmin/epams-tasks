﻿using System;


namespace _1._1._6.FONT_ADJUSTMENT {
    public class Program {
        private static void Main(string[] args) {
            Console.ForegroundColor = ConsoleColor.Green;
            FontsType font = FontsType.None;
            var myEnum = Enum.GetNames(typeof(FontsType));

            while (true) {
                DisplayMessages(ref font);
            }
        }

        private static void DisplayMessages(ref FontsType font) {
            Console.WriteLine("Current font type is {0}", font);
            Console.WriteLine("Введите:");
            Console.WriteLine("1: " + FontsType.Bold);
            Console.WriteLine("2: " + FontsType.Italic);
            Console.WriteLine("3: " + FontsType.Underline);
            AssignVariables(ref font);
            
        }

        private static void AssignVariables(ref FontsType font) {
            try {
                int fontType = int.Parse(Console.ReadLine());
                
                if (fontType > 4 || fontType < 0) {
                    throw new Exception();
                }

                switch (fontType) {
                    case 1:
                        font = FontsType.Bold;
                        break;
                    case 2:
                        font = FontsType.Italic;
                        break;
                    case 3:
                        font = FontsType.Underline;
                        break;
                }
            }
            catch (FormatException e) {
                Console.WriteLine("Wrong number");
            }
        }

        private enum FontsType {
            None = 0,
            Bold,
            Italic,
            Underline
        }
    }
}