﻿using System;
namespace Task_1._1._9 {
    internal class Program {
        public static void Main(string[] args) {
            const int Min = -100;
            const int Max = 100;

            var numArray = new int[100];

            var randNum = new Random();
            for (var i = 0; i < numArray.Length; i++) {
                numArray[i] = randNum.Next(Min, Max);
            }

            int sum = 0;
            foreach (var num in numArray) {
                if (num > 0) {
                    sum += num;
                }
            }
            Console.WriteLine(sum);
        }
    }
}